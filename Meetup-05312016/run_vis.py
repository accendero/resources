import csv
import numpy
from sklearn.decomposition import PCA
from sklearn.manifold import MDS, TSNE
import matplotlib
matplotlib.use("agg")
import matplotlib.pyplot as plt
import math

def roundup(x):
    return int(math.ceil(int(x) / 100.0)) * 100


def read_file(file_name, zipfield, features):
    zips = {}

    with open(file_name) as csvf:
        reader = csv.DictReader(csvf)
        for row in reader:
            ten_zip = roundup(row[zipfield])
            if not row[zipfield] in zips:
                zips[ten_zip] = [0] * len(features)
            for i, item in enumerate(features):
                zips[ten_zip][i] += int(row[item])

    zip_matrix = numpy.array([zips[key] for key in zips.keys()])
    return [int(one_zip) for one_zip in zips.keys()], zip_matrix

def make_scatter(zips, components, outputf):
    fig = plt.figure()
    sct = plt.scatter(
        components[:, 0], components[:, 1],
        c=zips, alpha=0.5, cmap=plt.cm.get_cmap('rainbow'))
    cbar = plt.colorbar(sct)
    plt.savefig(outputf)

if __name__ == "__main__":
    file_name = 'zbp13detail.txt'
    zipfield = "zip"
    features = ["n1_4","n5_9","n10_19","n20_49","n50_99","n100_249","n250_499","n500_999","n1000"]
    output_image = "zbp_tsne.png"

    zips, zip_matrix = read_file(file_name, zipfield, features)

    print("Transform started.")
    #Method here
    #algo = PCA(n_components=2)
    #transformed = algo.fit(zip_matrix).transform(zip_matrix)

    #algo = MDS(n_components=2)
    #transformed = algo.fit_transform(zip_matrix)

    algo = TSNE(n_components=2)
    transformed = algo.fit_transform(zip_matrix)

    make_scatter(zips, transformed, output_image)
